<?php

# The base remote dir for bups
# Use trailing slash!!
$remoteDir = "/path/to/dir/";

# Exclude pattern
# View more in rsync documentation
$excludePattern = "{*.sw*,.~lock.*}";

