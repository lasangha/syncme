#!/usr/bin/php

<?php

# System call
$rsync = "";

# The base remote dir for bups
$remoteDir = "/media/felipe/z/bups/";

# Exclude pattern
$excludePattern = "{*.sw*,.~lock.*}";

# Settings
include("settings.php");

# Files to backUp
$backThisUp = array();

# Include files to backup
include("backThisUpList.php");

system("clear");

print "/**********************************************************************/";
print "\n";
print "                         Welcome to the Sync!";
print "\n";
print "/**********************************************************************/";
print "\n";
print "\n";

# If there is something to backup, so be it!
if(count($backThisUp) > 0){
	print "Backing up!! \n";
	sync_bupFiles($backThisUp, $excludePattern, $remoteDir);
}else{
	print "Nothing to backup! \n\n\n";
}
/*
## Open config file
if(file_exists("bupList.sync")){

	print "Found some files to backup! \n";
	$backThisUp = file("bupList.sync");
	sync_bupFiles($backThisUp, $excludePattern, $remoteDir);

}else{
	print "Nothing to backup, file does not exist \n";
}
*/

/**
 * Backup the files
 *
 **/
function sync_bupFiles($files, $excludePattern, $remoteDir){
	foreach($files as $f){
		//print_r($f);
		echo "File: " . str_replace("\n", "", $f[0]) . " >> " . $f[1] . "\n";
		$command = sprintf("rsync -vurpgoPE --progress --delete --exclude=%s %s %s", $excludePattern, str_replace("\n", "", $f[0]), $f[1]);
		echo "Command: " . $command . "\n";
		system($command);
	}
}


